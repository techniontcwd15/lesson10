import { RouterSamplePage } from './app.po';

describe('router-sample App', () => {
  let page: RouterSamplePage;

  beforeEach(() => {
    page = new RouterSamplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
