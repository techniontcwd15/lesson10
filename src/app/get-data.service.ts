import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class GetDataService {

  constructor(private http: Http) { }

  getGetRequest() {
      return this.http.get('https://shielded-caverns-1626.herokuapp.com')
                      .map((res) => res.json())
                      .catch((error) => Observable.throw(error.json().error || 'Server error'));
  }

  getPostRequest(parameters) {
      return this.http.post('https://shielded-caverns-1626.herokuapp.com', parameters)
                      .map((res) => res.json())
                      .catch((error) => Observable.throw(error.json().error || 'Server error'));

  }

}
