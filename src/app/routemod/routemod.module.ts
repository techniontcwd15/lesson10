import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { Page1Component } from '../page1/page1.component';
import { Page2Component } from '../page2/page2.component';
import { Page3Component } from '../page3/page3.component';
import { Page4Component } from '../page4/page4.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';

const appRoutes: Routes = [
  { path: 'page1/:id', component: Page1Component },
  { path: 'page2', component: Page2Component, data: {headline: 'hello!'},
    children: [
      { path: 'jump-to-4', component: Page4Component }
    ] },
  { path: 'page3', component: Page3Component },
  { path: 'page4',   redirectTo: '/page3', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes
    )
  ],
  declarations: []
})
export class RoutemodModule { }
