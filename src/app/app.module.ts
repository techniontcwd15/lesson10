import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RoutemodModule } from './routemod/routemod.module';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { Page4Component } from './page4/page4.component';


@NgModule({
  declarations: [
    AppComponent,
    Page1Component,
    Page2Component,
    Page3Component,
    PageNotFoundComponent,
    Page4Component
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot([]),
    RoutemodModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
