import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetDataService } from '../get-data.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  providers: [GetDataService],
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {
  private response;

  constructor(private route:ActivatedRoute, private dataService:GetDataService) { }

  ngOnInit() {
    this.route.params
      .flatMap((parameters) => this.dataService.getPostRequest(parameters))
      .subscribe((data) => {
        this.response = data;
    });
  }

}
